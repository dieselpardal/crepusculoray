ANIMATOR CREPUSCULARES RAY 1.0 by Ivan Diesel
============

Version 1.0

DEV Ivan Rogerio Diesel

The objective is to compare the forms of realities of spherical and flat earth with precision measurement.

#### FLAT EARTH PROFILE

Ray: 15,962 km

Height: 5,000 km

Height of Cloud: 8km

Observer: 2 m


#### SPHERICAL EARTH
Ray: 6,371 km

Distance Sun 149,600,000 km

Height of Cloud: 8km

Observer: 2 m

---
java e openGL
IDE: Intellij IDEA Community

-------------------
##### PRESS KEY: 

  1) Globe: Earth in space
  
  2) Globe: Earth in land
  
  3) Flat: Earth in Dome - All Observe
  
  4) Flat: Earth in Land - All Observe
  
  5) Flat: Earth in Dome - Only Vertical
  
  6) Flat: Earth in Land - Only Vertical
  
  **Left button mouse**: Rotate
  
  **Right button mouse**: Translation XY
  
  **Middle button mouse**: Translação Z e girar zoom
  
  **C** - Cloud - Enable / Disable 
  
  **ESC**    - Exit.
  
---------------------------------------
